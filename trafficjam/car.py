class Car:
    def __init__(self, env, charge_duration=5, trip_duration=2):
        self.env = env
        self.action = env.process(self.run())
        self.charge_duration = charge_duration
        self.trip_duration = trip_duration

    def run(self):
        while True:
            print(f'Start parking and charging at {self.env.now}')
            yield self.env.process(self.charge(self.charge_duration))

            print(f'Start driving at {self.env.now}')
            yield self.env.timeout(self.trip_duration)

    def charge(self, duration):
        yield self.env.timeout(duration)
