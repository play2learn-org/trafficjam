import simpy

from trafficjam import car as car_pkg

def main():
    env = simpy.Environment()
    car1 = car_pkg.Car(env)
    env.run(until=15)
    pass

if __name__ == '__main__':
    main()
