import setuptools

import sys

def read(file):
    with open(file, 'r') as f:
        return f.read()

project = "trafficjam"
version = "1.0"

if sys.argv[-1] == 'publish':
    sys.argv = [sys.argv[0], 'sdist', 'bdist_wheel', 'upload']

setuptools.setup(
    name="trafficjam",
    version="1.0",
    description="example one-dimensional traffic jam simulation with simpy",
    author="Erik Bernoth",
    author_email="monimagspielen@gmx.de",
    packages = ["trafficjam"],
    license=read("LICENSE.txt"),
    install_requires = [
        'simpy>=4.0.2',
    ],provides = [
        f'{project} ({version})',
    ],
)
